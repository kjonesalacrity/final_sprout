<?php

namespace App;
use App\Progression;
use App\User;
use App\Phase;

use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
    protected $fillable = [
        'name', 'start_date', 'end_date', 'phase_id', 'time_in',
    ];

    // public function user() {
    //   return $this->belongsTo(User::class);
    // }
    protected $dates = [
      'end_date'
    ];

    public function progressions(){
   		return $this->hasMany(Progression::class);
    }

    

    public function getDaysLeft(){
      $currentDate = date("Y-m-d");

      $daysDone = (strtotime($currentDate) - strtotime($this->start_date)) / (60 * 60 * 24);
      
      $overallDays = ( strtotime($this->end_date) - strtotime($this->start_date)) / (60 * 60 * 24);

      $daysLeft = $overallDays - $daysDone;

      if($daysDone < 0){
        return 'TO COME';
      }
      else if(($daysLeft) < 0){
        return 'DONE';
      }
      else
        return $daysLeft;
     
    }

    // return the latest, and therefore, most up to date progress report (if one exists)
    public function currentProgression() {
      return $this->progressions()->latest()->first();
    }

    public function phase(){
      return $this->belongsTo(Phase::class);
    }
}