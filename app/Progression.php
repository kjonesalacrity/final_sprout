<?php

namespace App;

use App\Target;

use Illuminate\Database\Eloquent\Model;

class Progression extends Model
{
    protected $fillable = [
        'target_id', 'user_id', 'progress', 'timein', 'updated',
    ];

    protected $dates = [
    	'updated'
    ];

    public function getRestPrecent(){
        return (100 - $this->progress);
    }

    public function target(){
    	return $this->belongsTo(Target::class);
    }

    public function user(){
    	return $this->belongsTo(User::class);
    }

}