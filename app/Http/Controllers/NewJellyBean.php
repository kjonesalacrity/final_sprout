<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class NewJellyBean extends Controller
{
    //
    public function updateJelly(){
    	User::where('id', auth()->user()->id)->update(array('jellyName' => request('jellyName'), 'jellyColor' => request('jellyColor'), 'jellyType' => request('jellyType')));

      	return;
    }
}
