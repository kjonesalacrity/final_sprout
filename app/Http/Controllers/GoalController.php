<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Goal;

class GoalController extends Controller
{
    //
    public function store()
    {
    	$goal = new Goal;

    	$goal->user_id = Auth::user()->id;
    	$goal->name = request('goal_name_personal');
    	$goal->progress = 0;
    	$goal->start_date = request('start_date');
    	$goal->end_date = request('end_date');

    	$goal->save();

    	return redirect()->route('home');
    } 

    public function updateGoals(){
    	$goals = Goal::where('user_id', auth()->user()->id)->get();

    	foreach ($goals as $i => $goal){
    		$goal->update(array('progress' => request('updategoal_progress'.$i),
	    							 'start_date' => request('updategoal_startdate'.$i), 
	    							 'end_date' => request('updategoal_enddate'.$i)
	    							)
	    					);
    	}
    	

    	return redirect()->route('home');
    }
}
