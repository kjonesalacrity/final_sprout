<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

use App\Phase;
use App\User;
use App\Target;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentDate = date("Y-m-d");

        $currentPhase = Phase::where([
                        ['start_date', '<', $currentDate],
                        ['end_date', '>', $currentDate],
                        ])->first();


        //CHECK THAT THERE IS A PHASE AT THIS CURRENT MOMENT
        if($currentPhase == null){
            return view('nothing');
        }

        //GETTING THE PERCENTAGE OF TIME PASSED
        $daysDone = (strtotime($currentDate) - strtotime($currentPhase->start_date)) / (60 * 60 * 24);
        $overallDays = ( strtotime($currentPhase->end_date) - strtotime($currentPhase->start_date)) / (60 * 60 * 24);

        $percentDays = round($daysDone /  $overallDays * 100);

        $currentTargets = Target::where('phase_id', $currentPhase->id)->get();

        //CHECK IF THE USER IS AN ADMIN OR NOT
        if((Auth::user()->email == 'alacrity@admin.com')){

            //DISPLAYING ALL USERS THAT ARE NOT ADMIN
            $users = User::where('jellyName', '<>', '')->get();

            //SEND THEM TO THE MANAGEMENT PAGE WHERE ALL THE BEANS WILL BE SHOWED
            return view('managment', compact('users', 'currentPhase', 'percentDays', 'currentTargets'));

        }else{

            

            $userProgressions = Auth::user()->progressions()->get();

            if(Auth::user()->id > 2){
                $newuser = true;
            }
            else
                $newuser = false;

         

            return view('home', compact('currentTargets', 'userProgressions', 'currentPhase', 'percentDays', 'newuser'));

        }
    }

}
                                        
