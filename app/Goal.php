<?php

namespace App;

use App\User;

use Illuminate\Database\Eloquent\Model;

class Goal extends Model
{
    protected $fillable = [
        'user_id', 'name', 'progress', 'start_date', 'end_date',
    ];

    protected $dates = [
      'start_date','end_date' 
    ];


    public function user(){
    	return $this->belongsTo(User::class);
    }

    public function getRestPrecent(){
    	return (100 - $this->progress);
    }

    public function getDaysLeft(){
      $currentDate = date("Y-m-d");

      $daysDone = (strtotime($currentDate) - strtotime($this->start_date)) / (60 * 60 * 24);
      
      $overallDays = ( strtotime($this->end_date) - strtotime($this->start_date)) / (60 * 60 * 24);

      $daysLeft = $overallDays - $daysDone;

      if($daysDone < 0){
        return 'TO COME';
      }
      else if(($daysLeft) < 0){
        return 'DONE';
      }
      else
        return $daysLeft;
     
    }
    
} 