<?php

namespace App;

use App\Target;

use Illuminate\Database\Eloquent\Model;

class Phase extends Model
{
    protected $fillable = [
        'name', 'start_date', 'end_date', 
    ];

    public function targets(){
    	return $this->hasMany(Target::class);
    }

}