<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Show the page for the create new bean
Route::get('/createBean', 'View@newJelly')->name('create.newbean');
Route::post('/updateJelly', 'NewJellyBean@updateJelly')->name('update.newbean');

//Add new goal
Route::post('newgoal', 'GoalController@store')->name('store.newgoal');
Route::post('goalupdate', 'GoalController@updateGoals')->name('update.goal');