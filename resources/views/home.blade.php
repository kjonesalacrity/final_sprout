@extends('layouts.master')

@section('body')
    <nav class="nav flex-column" id="sideNav">
        <a class="nav-link links" href=""><img src="{{ asset('images/bean_logo.svg') }}" class="bean_logo"></a>
        
        <a class="nav-link links" href="{{ route('home') }}">HOME</a>
        <a class="nav-link links" href="#">ALACRITY</a>
        

        @if (Auth::check())
            <a class="nav-link links" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                    {{ __('LOGOUT') . " " . Auth::user()->name }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        @endif
    </nav>

    <div class="beans"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 "></div>
                <div class="col-lg-5 " id="bean_homepage">
                    <br>
                    <div id="big_bean_homepage"> 
                        @if( Auth::user()->jellyType == 'hipster')
                            @if ( Auth::user()->jellyColor == 'pink')
                                <img src="images/pink_bean_hipster.gif" class="front_bean">
                            @elseif ( Auth::user()->jellyColor == 'green')
                                <img src="images/green_bean_hipster.gif" class="front_bean">
                            @elseif ( Auth::user()->jellyColor == 'blue')
                                <img src="images/blue_bean_hipster.gif" class="front_bean">
                            @else
                                <img src="images/orange_bean_hipster.gif" class="front_bean">
                            @endif
                        @elseif( Auth::user()->jellyType == 'normal')
                            @if ( Auth::user()->jellyColor == 'pink')
                                <img src="images/pink_bean.gif" class="front_bean">
                            @elseif ( Auth::user()->jellyColor == 'green')
                                <img src="images/green_bean.gif" class="front_bean">
                            @elseif ( Auth::user()->jellyColor == 'blue')
                                <img src="images/blue_bean.gif" class="front_bean">
                            @else
                                <img src="images/orange_bean.gif" class="front_bean">
                            @endif
                        @endif
                        <br>
                        <img src="images/bean_shaddow.png" class="front_bean_shaddow">
                    </div>
                    @if (Auth::check())
                        <p class="jellyName_homepage"> {{ Auth::user()->jellyName }}</p>
                        <p> {{ Auth::user()->name }}</p>

                    @endif
                    <img class="teamp_home" src="images/teamp.png">
                </div>
                <div class="col-lg-6 col-md-2 col-sm-3">
                    <br>
                    <div class="row">
                        <div class="col-lg-12 col-md-2 col-sm-3">
                            <div id="graph_div">
                                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                  <div class="carousel-inner">

                                        @foreach ($currentTargets as $i => $target )
                                            <div class="carousel-item">
                                                <?php $i++; ?>
                                                <script type="text/javascript">
                                                    var count = 0;
                                                    var countWeek = 0;
                                                    var dataProgress = [];
                                                    var dataUpdated = [];

                                                    if( "{{ $i }}" == "1"){
                                                        $(".carousel-item").addClass('active');
                                                    }
                                                </script>
                                                @foreach(Auth::user()->getProgressionsForTarget($target) as $targetProgression)
                                                    <script type="text/javascript"> 
                                                         dataProgress[count++] = "{{ $targetProgression->progress }}";
                                                         dataUpdated[countWeek++] = "{{ $targetProgression->updated->toFormattedDateString() }}";
                                                    </script>
                                                @endforeach
                                               

                                                <canvas id="line-chart{{ $i }}" class="homepage-graphs" height="130vh"></canvas> 

                                                <script type="text/javascript">
                                                    new Chart(document.getElementById("line-chart{{ $i }}"), {
                                                        type: 'line',
                                                        data: {
                                                          labels: dataUpdated,
                                                          datasets: [{ 
                                                              data: dataProgress,
                                                              label: "Yours",
                                                              borderColor: "#EC008B",
                                                              fill: false
                                                            }, { 
                                                              data: [0,20,70,100],
                                                              label: "Average",
                                                              borderColor: "#F16521",
                                                              fill: false
                                                            }
                                                          ]
                                                        },
                                                        options: {
                                                          title: {
                                                            display: true,
                                                            text: "{{ $currentPhase->name }} : {{ $target->name }}"
                                                          }
                                                        }
                                                      });
                                                </script>
                                            </div>
                                           
                                        @endforeach
                                  </div>
                                  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                  </a>
                                  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                  </a>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <br>
                    
                    <div class="row">
                        <div class="col-lg-6 col-md-2 col-sm-3">
                            <!-- WE ARE GOING TO LOOP THROUGH ALL GOALS AND GIVE THME IN A ROW -->
                            <div class="list_div">
                                <div class="headings_other">Alacrity Goals</div>
                                <h6>{{ $currentPhase->name }}</h6>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: {{ $percentDays }}%;" aria-valuenow="{{ $percentDays }}" aria-valuemin="0" aria-valuemax="100">{{ $percentDays }}%</div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-lg-6 ">
                                        <h4> GOAL NAMES: </h4>
                                    </div>
                                    <div class="col-lg-3 ">
                                        <h4> PROGRESS: </h4>
                                    </div>
                                    <div class="col-lg-3 ">
                                        <h4> TIME LEFT: </h4>
                                    </div>
                                </div>
                                <br>

                                @foreach ($currentTargets as $j => $target)
                                    <?php $j++; ?>
                                     <div class="row">
                                            <div class="col-lg-6 ">
                                                <div class="goalname">  {{ $target->name }} </div>
                                            </div>
                                            <div class="col-lg-3 ">

                                                <!-- PROGRESS OF THAT GOAL -->

                                                <script type="text/javascript">var tprogessHomepage = 0; var tprogressLeftHompepage = 0;</script>
                                               
                                                @foreach(Auth::user()->getProgressionsForTarget($target) as $targetProgression)
                                                    <script type="text/javascript">
                                                        var tprogessHomepage = "{{ $targetProgression->progress }}";
                                                        var tprogressLeftHompepage = "{{ $targetProgression->getRestPrecent() }}";
                                                    </script>   
                                                @endforeach


                                                
                                                <canvas id="doughnut-chart{{ $j }}" class="d_charts" width="8" height="8"></canvas> 
                                                <script type="text/javascript">

                                                    new Chart(document.getElementById("doughnut-chart{{ $j }}"), {
                                                      type: 'doughnut',
                                                      data: {
                                                        labels: [],
                                                        datasets: [
                                                          {
                                                            label: tprogessHomepage,
                                                            backgroundColor: ["#EC008B", "#FFF"],
                                                            borderWidth: ['0px', '0px'],
                                                            data: [tprogessHomepage,tprogressLeftHompepage]
                                                          }
                                                        ]
                                                      },
                                                      options: {
                                                        title: {
                                                          display: false,
                                                          text: 'Predicted world population (millions) in 2050'
                                                        },
                                                        cutoutPercentage: 90
                                                      }
                                                  });
                                                </script>
                                                <div class="target_progress"><script type="text/javascript">document.write(tprogessHomepage)</script> %</div>
                                                <br>
                                                 
                                            </div>
                                            <div class="col-lg-3 ">
                                              
                                                <!-- TIME LEFT OF THAT GOAL -->
                                                <!-- <canvas id="doughnut-chart2" width="8" height="8"></canvas>     -->
                                              
                                                
                                                {{ $target->getDaysLeft() }}
                                            </div>
                                        </div>
                                        <br>
                                @endforeach
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-2 col-sm-3">
                            <!-- WE ARE GOING TO LOOP THROUGH ALL GOALS AND GIVE THME IN A ROW -->
                            <div class="list_div">
                                <div class="headings_other">Personal Goals</div>
                                <br><br>

                                <div class="row">
                                    <div class="col-lg-6 ">
                                        <h4> GOAL NAMES: </h4>
                                    </div>
                                    <div class="col-lg-3 ">
                                        <h4> PROGRESS: </h4>
                                    </div>
                                    <div class="col-lg-3 ">
                                        <h4> TIME LEFT: </h4>
                                    </div>
                                </div>
                                <br>

                                @foreach ( Auth::user()->goals as $h => $goal)
                                    <?php $h++; ?>
                                    <div class="row">
                                        <div class="col-lg-6 ">
                                            <div class="goalname">  {{ $goal->name }} </div>
                                        </div>
                                        <div class="col-lg-3 ">
                                            <!-- PROGRESS OF THAT GOAL -->
                                            <canvas id="doughnut-chart-goals{{ $h }}" class="d_charts" width="8" height="8"></canvas>  
                                            <script type="text/javascript">
                                                    new Chart(document.getElementById("doughnut-chart-goals{{ $h }}"), {
                                                      type: 'doughnut',
                                                      data: {
                                                        labels: [],
                                                        datasets: [
                                                          {
                                                            label: "{{ $goal->name }}",
                                                            backgroundColor: ["#F16521", "#FFF"],
                                                            borderWidth: ['0px', '0px'],
                                                            data: ["{{ $goal->progress }}", "{{ $goal->getRestPrecent() }}"]
                                                          }
                                                        ]
                                                      },
                                                      options: {
                                                        title: {
                                                          display: false,
                                                          text: 'Predicted world population (millions) in 2050'
                                                        },
                                                        cutoutPercentage: 90
                                                      }
                                                  });
                                            </script>
                                            <div class="target_progress">{{ $goal->progress }} %</div>
                                            <br>

                                        </div>
                                        <div class="col-lg-3 ">
                                            <!-- TIME LEFT OF THAT GOAL -->
                                            <!-- <canvas id="doughnut-chart2" width="8" height="8"></canvas>  -->
                                            {{ $goal->getDaysLeft() }}
                                        </div>
                                    </div>
                                
                                @endforeach

                                <br>
                                <a data-toggle="modal" data-target="#myModal" class="buttons">ADD NEW GOAL</a>
                                <a data-toggle="modal" data-target="#myModalUpdate" class="buttons">UPDATE GOALS</a>
                                <br>

                                <!-- MODAL FOR TO ADD NEW GOAL -->
                                <div class="modal fade" id="myModal" role="dialog">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            
                                            <div class="heading_form">Add a new personal goal </div>
                                            <form class="personal_form" method="POST" action="{{ route('store.newgoal') }}">
                                                @csrf

                                                <label for="email_login">GOAL NAME: </label>
                                                <input type="text" name="goal_name_personal" id="goal_name_personal"  required>

                                                <label for="pass_login">START DATE: </label>
                                                <input type="date" name="start_date" id="start_date" class="dates" required>
                                                
                                                <label for="pass_login">END DATE: </label>
                                                <input type="date" name="end_date" id="end_date" class="dates" required>

                                                <br><br>
                                                <input type="submit" name="submit_login" id="submit_login" value="SUMBIT">
                                            </form>

                                            <img src="images/bean_bluegreen.svg" class="bean_background">
                                        </div>
                                    </div>
                                </div>

                                <!-- MODAL FOR TO ADD UPDATING A GOAL -->
                                <div class="modal fade" id="myModalUpdate" role="dialog">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="heading_form">Update your goals </div><br>
                                                <div class="update_body">
                                                    <div class='row'>
                                                         <div class="col-lg-3 col-md-2 col-sm-3">Goal Name</div>   
                                                         <div class="col-lg-1 col-md-2 col-sm-3">Current Progress</div>  
                                                         <div class="col-lg-2 col-md-2 col-sm-3">Start Date</div>  
                                                         <div class="col-lg-2 col-md-2 col-sm-3">End Date</div>  
                                                         <div class="col-lg-2 col-md-2 col-sm-3">Days Left</div>  
                                                    </div>
                                                    <form class="" method="POST" action="{{ route('update.goal') }}" >
                                                        @csrf

                                                        @foreach ( Auth::user()->goals as $k => $goal)
                                                           
                                                            <div class='row'>
                                                                <div class="col-lg-3 col-md-2 col-sm-3"> {{ $goal->name }}</div>   
                                                                <div class="col-lg-1 col-md-2 col-sm-3"> 
                                                                     <input type="text" name="updategoal_progress{{ $k }}" id="updategoal_progress{{ $k }}"  value= "{{ $goal->progress }}" >
                                                                </div>  

                                                                <div class="col-lg-2 col-md-2 col-sm-3"> 
                                                                    <input type="date" name="updategoal_startdate{{ $k }}" id="updategoal_startdate{{ $k }}" class="" value= "{{ $goal->start_date->toDateString() }}" >
                                                                </div>  

                                                                <div class="col-lg-2 col-md-2 col-sm-3"> 
                                                                    <input type="date" name="updategoal_enddate{{ $k }}" id="updategoal_enddate{{ $k }}" class="" value= "{{ $goal->end_date->toDateString() }}" >
                                                                </div>  

                                                                <div class="col-lg-2 col-md-2 col-sm-3"> {{ $goal->getDaysLeft() }}
                                                                </div>  
                                                            </div>
                                                        @endforeach
                                                        <input type="submit" name="submit_login" id="submit_login" value="SUMBIT">
                                                    </form>
                                                    <!-- <form class="personal_form" >
                                                        @csrf

                                                        <label for="email_login">GOAL NAME: </label>
                                                        <input type="text" name="goal_name_personal" id="goal_name_personal"  required>

                                                        <label for="pass_login">START DATE: </label>
                                                        <input type="date" name="start_date" id="start_date" class="dates" required>
                                                        
                                                        <label for="pass_login">END DATE: </label>
                                                        <input type="date" name="end_date" id="end_date" class="dates" required>

                                                        <br><br>
                                                        <input type="submit" name="submit_login" id="submit_login" value="SUMBIT">
                                                    </form> -->
                                                </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-12 col-md-2 col-sm-3">
                            <div id="comments-div">
                                <div class="headings_other">Comments</div>
                                @if($newuser)

                                @else
                                    <ul class="comments_list">
                                        <hr>
                                        <img src="images/admin.svg" class="admin_icon">
                                        <li class="admin_headin">Admin</li>
                                        <li class="admin_days">7 Days ago</li>
                                        <br>
                                        <li class="comment_words">Just a small suggestion, maybe add some more time into getting your projects done in time rather than rushing all of it at the last minute.</li>
                                        <hr>
                                        <img src="images/admin.svg" class="admin_icon">
                                        <li class="admin_headin">Admin</li>
                                        <li class="admin_days">10 Days ago</li>
                                        <br>
                                        <li class="comment_words">Nice work kelly! You are doing well but just remember to add some time to netwroking. Maybe add it as a personal goal.</li>

                                        <hr>
                                        <img src="images/admin.svg" class="admin_icon">
                                        <li class="admin_headin">Admin</li>
                                        <li class="admin_days">30 Days ago</li>
                                        <br>
                                        <li class="comment_words">Welcome to the bootcamp stage Kelly, I hope that you are feeling welcome and ready to get to it! We all have faith that you will crush it, jus do not dissapoint us okay!</li>
                                     </ul>
                                 @endif
                                 
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-12 col-md-2 col-sm-3">
                            <div id="graph_div">
                                <div class="headings_other">Time in Alacrity</div>
                                <h6 class="centered"> The overall hours you have put into for this phase. </h6>
                                <br>
                                    <div class="row centered">
                                        <?php $overall = 0; $overallTime = 0;?>
                                         @foreach ($currentTargets as $x => $target )
                                            <div class="col-lg-3 ">
                                                
                                            
                                               
                                               @foreach( Auth::user()->getProgressionsForTarget($target) as $targetProgression)
                                                    <?php 
                                                        $overall += $targetProgression->progress; 
                                                        $overallTime += $targetProgression->timein; 
                                                    ?> 
                                                @endforeach

                                                

                                                <?php $overall = round($overall/$j); ?>

                                                <script type="text/javascript"> 
                                                    var overalltime = "<?php echo $overallTime ?> "; 
                                                </script>
                                                
                                                <canvas id="doughnut-chart-time{{ $x }}" class="time_charts" width="8" height="8"></canvas> 
                                                    <script type="text/javascript">

                                                        new Chart(document.getElementById("doughnut-chart-time{{ $x }}"), {
                                                          type: 'doughnut',
                                                          data: {
                                                            labels: [],
                                                            datasets: [
                                                              {
                                                                label: overalltime,
                                                                backgroundColor: ["#8cc63e", "#FFF"],
                                                                borderWidth: ['0px', '0px'],
                                                                data: [overalltime,"{{ $target->time_in }}"]
                                                              }
                                                            ]
                                                          },
                                                          options: {
                                                            title: {
                                                              display: false,
                                                              text: 'Predicted world population (millions) in 2050'
                                                            },
                                                            cutoutPercentage: 90
                                                          }
                                                      });
                                                    </script>
                                                 
                                                <div class="percent_time">
                                                    <script type="text/javascript">document.write(overalltime)</script> 
                                                    / {{ $target->time_in }} 
                                                </div>
                                                <br>
                                                <div class="goalname_time">  {{ $target->name }} </div>
                                            
                                                <br>
                                            </div>
                                         @endforeach 
                                    </div>   
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
@endsection
