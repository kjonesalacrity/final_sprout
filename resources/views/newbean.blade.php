@extends('layouts.master')

@section('body')
	<div class="beans">
		<div class="container-fluid">
			<div class="formTops">	
				<!--FORM -->
				<div class="row">
					<div class="col-lg-1 choices">
						<div id="color_choice">
							<div class="color_bean point_active" id="pink"></div>
							<div class="color_bean" id="orange"></div>
							<div class="color_bean" id="blue"></div>
							<div class="color_bean" id="green"></div>
						</div>
					</div>
					<div class="col-lg-1" id="bean_type_choices">
						<div id="type_choice">
							<div class="type_bean point_active" id="normal"></div>
							<div class="type_bean" id="hipster"></div>
						</div>
					</div>
					<div class="col-lg-3">
						<img src="images/pink_bean.gif" class="new_bean_front">
		 				<br>
		 				<img src="images/bean_shaddow.png" class="new_bean_front_shaddow">
					</div>
				</div>	

				<div class="row">
					<div class="col"></div>
						<div class="col-lg-4 col-md-8 col-sm-5" >
							<form action="" method="POST" id="new_bean_form">
								@csrf

								<label for="jelly_new">JELLY NAME: </label>
								<input type="text" name="jelly_new" id="jelly_new">

								<br><br>
								<a class="buttons" id="new_bean_submit">SUMBIT</a>
							</form>
								
						</div>
					<div class="col"></div>
				</div>	
			</div>
		</div>
	</div>
@endsection