@extends('layouts.master')

@section('body')
    <div id="inspage">
        <div class="container-fluid">
            <div class="formTops">  
                <!--FORM -->    
                <div class="row">
                    <div class="col"></div>
                    <div class="col-lg-6 col-md-8 col-sm-12" id="form_outline_login">
                        <div class="row">
                            <div class="col-lg-1 col-md-4 col-sm-1"></div>
                            <div class="col-lg-3">
                                <h1>Sprout</h1>
                            </div>
                        </div>

                        

                        <div class="row">
                            <div class="col-lg-1 col-md-4 col-sm-1"></div>
                            <div class="col-lg-7 col-md-8 col-sm-6" >
                                <form method="POST" action="{{ route('register') }}">
                                    @csrf


                                    <label for="name">{{ __('NAME:') }}</label>

                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"  name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                               
                                    <label for="email" class="email_signup">{{ __('EMAIL:') }}</label>

                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} email_signup"name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                
                                    <label for="password" class="pass_signup" >{{ __('PASSWORD:') }}</label>

                                  
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} pass_signup" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                    <br>
                                    <button type="submit" class="submit_signup">
                                        {{ __('SIGN UP') }}
                                    </button>
                                       
                                    
                                </form>
                                
                            </div>
                            <div class="col"></div>
                        </div>
                    </div>
                    <div class="col"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
