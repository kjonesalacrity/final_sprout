@extends('layouts.master')

@section('body')
    <div id="inspage">
        <div class="container-fluid">
            <!-- ADD ERROR HERE -->
            <div class="formTops">  
                
                <!--FORM -->    
                <div class="row">
                    <div class="col"></div>
                    <div class="col-lg-6 col-md-8 col-sm-12" id="form_outline_login">
                        <div class="row">
                            <div class="col-lg-1 col-md-4 col-sm-1"></div>
                            <div class="col-lg-3">
                                <h1>Sprout</h1>
                            </div>
                        </div>

                        <br><br>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-1"></div>
                            <div class="col-lg-7 col-md-8 col-sm-5" >
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf

                                    <label for="email" id='lables' >{{ __('EMAIL:') }}</label>
                                    <br/>
                                   
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                              

                               
                                    <label for="password" >{{ __('PASSWORD:') }}</label>
                                    <br/>
                                   
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif

                                    <br>
                              
                                    <button type="submit"  id="submit_login">
                                        {{ __('LOGIN') }}
                                    </button>

                                    <a class="link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                  
                                </form>
                                
                                
                                
                            </div>
                            <div class="col"></div>
                        </div>
                    </div>
                    <div class="col"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
