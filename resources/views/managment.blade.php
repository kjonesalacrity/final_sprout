@extends('layouts.master')

@section('body')
    <nav class="nav flex-column" id="sideNav">
        <a class="nav-link links" href=""><img src="{{ asset('images/bean_logo.svg') }}" class="bean_logo"></a>
        
        <a class="nav-link links" href="{{ route('home') }}">HOME</a>
        <a class="nav-link links" href="#">TARGETS</a>

        @if (Auth::check())
            <a class="nav-link links" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                    {{ __('LOGOUT') . " " . Auth::user()->name }}
            </a>
            <br>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        @endif
    </nav>

	<div class="beans"></div>
		<div class="container-fluid">
			<img src="images/yourbeans.svg" class='header_management'>
		 	<div class="row">
		 		<div class="col-lg-1 col-md-2 col-sm-3">	
		 		</div>
		 		<div class="col-lg-11 col-md-2 col-sm-3">
		 			<!-- DYNAMICALLY ADD A ROW EACH TIME -->
		 			<div class='row'>
		 				@foreach ($users as $i => $user)
							<div class="col-md-3" id="bean_pods" data-toggle="modal" data-target="#myModal{{ $user->id }}" >
							    <div class="card user-info" id="{{ $user->id }}">
							    	<br>
							    	<div class="row">
							    		<div class="col-lg-12">
							    			<p class="user_name_mang">{{ $user->name }}</p><br>
							    		</div>
							    	</div>
							    	<div class='row'>
							    		<div class="col-md-6 col-sm-12">
							    			<div class="bean_pic_mang">
							    				@if( $user->jellyType == "hipster")
								 					@if ( $user->jellyColor == 'pink')
								 						<img src="images/pink_bean_hipster.gif" class="front_bean_mang">
								 					@elseif ( $user->jellyColor == 'green')
								 						<img src="images/green_bean_hipster.gif" class="front_bean_mang">
								 					@elseif ( $user->jellyColor == 'blue')
								 						<img src="images/blue_bean_hipster.gif" class="front_bean_mang">
								 					@else
								 						<img src="images/orange_bean_hipster.gif" class="front_bean_mang">
								 					@endif
								 				@elseif( $user->jellyType == 'normal')
								 					@if ( $user->jellyColor == 'pink')
								 						<img src="images/pink_bean.gif" class="front_bean_mang">
								 					@elseif ( $user->jellyColor == 'green')
								 						<img src="images/green_bean.gif" class="front_bean_mang">
								 					@elseif ( $user->jellyColor == 'blue')
								 						<img src="images/blue_bean.gif" class="front_bean_mang">
								 					@else
								 						<img src="images/orange_bean.gif" class="front_bean_mang">
								 					@endif
								 				@endif
							    			</div>
							    			<p class="user_jellyname_mang">{{ $user->jellyName }}</p>
							    		</div>
							    		<div class="col-md-6 col-sm-12">
							    			<p class="phase_name_mang">Current Phase: </p>
							    			<p class="phase_mang"> {{ $currentPhase->name }} </p>
							    					    			
							    			<div class="progress">
  												<div class="progress-bar" role="progressbar" style="width: {{ $percentDays }}%;" aria-valuenow=" aria-valuemin="0" aria-valuemax="100">{{ $percentDays }}%</div>
											</div>
											<br>
											<img class="teamp" src="images/teamp.png">
										</div>
							    	</div>
							    	<br><br>
							    	<div class='row centered'>
							    		  <div class="col-lg-12 col-sm-12">
							    				Overall progress of this phase:
							    			</div>
							    	</div>
							    	<div class='row centered progress_mang'>
										<?php $overall = 0; $overallTime = 0;?>
										@foreach ($currentTargets as $j => $target)
		                                         
                                            @foreach($user->getProgressionsForTarget($target) as $targetProgression)
                                                <?php 
                                                	$overall += $targetProgression->progress; 
                                                	$overallTime += $targetProgression->timein; 
                                                ?> 
                                            @endforeach
                                         
		                                @endforeach

		                                <div class="col-lg-6">
		                                	<?php $overall = round($overall/10); ?>
		                                	<script type="text/javascript"> 
		                                		var overall = "<?php echo $overall ?> "; 
		                                		var rest = 100 - overall;
		                                	</script>
		                                	
		                                	<canvas id="doughnut-chart{{ $i }}" class="man_charts" width="8" height="8"></canvas> 
                                                <script type="text/javascript">

                                                    new Chart(document.getElementById("doughnut-chart{{ $i }}"), {
                                                      type: 'doughnut',
                                                      data: {
                                                        labels: [],
                                                        datasets: [
                                                          {
                                                            label: overall,
                                                            backgroundColor: ["#EC008B", "#FFF"],
                                                            borderWidth: ['0px', '0px'],
                                                            data: [overall,rest]
                                                          }
                                                        ]
                                                      },
                                                      options: {
                                                        title: {
                                                          display: false,
                                                          text: 'Predicted world population (millions) in 2050'
                                                        },
                                                        cutoutPercentage: 90
                                                      }
                                                  });
                                                </script>
                                             <div class="target_progress mang_tarts"><script type="text/javascript">document.write(overall)</script> %</div>
							    		</div>
										<div class="col-lg-6">
		                                	<?php $overalltime = round($overallTime); ?>
		                                	<script type="text/javascript"> 
		                                		var overalltime = "<?php echo $overalltime ?> "; 
		                                		var resttime = 100 - overalltime;
		                                	</script>
		                                	
		                                	<canvas id="doughnut-chart_other{{ $i }}" class="man_charts" width="8" height="8"></canvas> 
                                                <script type="text/javascript">

                                                    new Chart(document.getElementById("doughnut-chart_other{{ $i }}"), {
                                                      type: 'doughnut',
                                                      data: {
                                                        labels: [],
                                                        datasets: [
                                                          {
                                                            label: overalltime,
                                                            backgroundColor: ["#F16521", "#FFF"],
                                                            borderWidth: ['0px', '0px'],
                                                            data: [overalltime,"{{ $target->time_in }}"]
                                                          }
                                                        ]
                                                      },
                                                      options: {
                                                        title: {
                                                          display: false,
                                                          text: 'Predicted world population (millions) in 2050'
                                                        },
                                                        cutoutPercentage: 90
                                                      }
                                                  });
                                                </script>
                                              <div class="percent_time_mang">
                                                    <script type="text/javascript">document.write(overalltime)</script> 
                                                    / {{ $target->time_in }} 
                                                </div>    
							    		</div>	                                          
							    	</div>
							    	<div class="row centered lables_mang">
							    		<div class="col-lg-6">
							    			<div >PROGRESS</div>	
							    		</div>
							    		<div class="col-lg-6">
							    			<div >TIME IN</div>	
							    		</div>
							    	</div>	
							    	<br>
							    	<div class='row'>
							    		<div class="col-lg-12">
							    			<div class="button_mang">UPDATE</div>	
							    		</div>
							    	</div>
							    	<br>
								</div>
							</div>

							<!-- Modal -->
							<div class="modal fade" id="myModal{{ $user->id }}" role="dialog">
							    <div class="modal-dialog modal-lg">
							        <div class="modal-content">
                                            <div class="heading_form">Update your bean information </div><br>
                                                <div class="update_body">
                                                	<div class='row centered'>
                                                        <div class="col-lg-12 col-md-2 col-sm-3 headings_other"> {{ $user->name }}</div><br><br>
                                                     </div>
                                                    <div class='row'>
                                                         <div class="col-lg-4 col-md-2 col-sm-3 bolder">Target Name</div>   
                                                         <div class="col-lg-2 col-md-2 col-sm-3 bolder" >Current Progress</div>  
                                                         <div class="col-lg-2 col-md-2 col-sm-3 bolder">Start Date</div>  
                                                         <div class="col-lg-2 col-md-2 col-sm-3 bolder">Days Left</div>  
                                                    </div><br>
                                                    <form class="" method="POST" action="" >
                                                        @csrf

                                                        @foreach ($currentTargets as $u => $target )
                                                        	<div class='row centered'>
                                                        		<div class="col-lg-12 col-md-2 col-sm-3"> {{ $currentPhase->name }}</div><br><br>
                                                        	</div>
                                                        	@foreach($user->getProgressionsForTarget($target) as $targetProgression)
                                                        		<div class='row'>
	                                                                <div class="col-lg-4 col-md-2 col-sm-3"> {{ $target->name }}</div>   
	                                                                <div class="col-lg-2 col-md-2 col-sm-3"> 
	                                                                     <input type="text" name="updategoal_progress{{ $u }}" id="updategoal_progress{{ $u }}"  value= "{{ $targetProgression->progress }}" >
	                                                                </div>  

	                                                                <div class="col-lg-2 col-md-2 col-sm-3"> 
	                                                                    <input type="date" name="updategoal_startdate{{ $u }}" id="updategoal_startdate{{ $u }}" class="" value= "{{ $targetProgression->updated->toDateString() }}" >
	                                                                </div>  

	                                                                <div class="col-lg-2 col-md-2 col-sm-3"> {{ $target->getDaysLeft() }}
	                                                                </div>  
	                                                            </div>
			                                                   
			                                                @endforeach
			                                                <hr>
                                                        @endforeach

                                                       
                                                        <input type="submit" name="submit_login" class="button_mang" value="SUMBIT">
                                                    </form>
                                                </div>
                                        </div>
							    </div>
							</div>
						@endforeach
		 			</div>
		 		</div>
		 	</div>
		 	<br>
		</div>
@endsection