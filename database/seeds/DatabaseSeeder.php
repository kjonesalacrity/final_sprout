<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(GoalsTableSeeder::class);
         $this->call(PhasesTableSeeder::class);
         $this->call(ProgressionsTableSeeder::class);
         $this->call(TargetTableSeeder::class);
    }
}
