<?php

use Illuminate\Database\Seeder;

class PhasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	
		DB::table('phases')->insert([
            'name' => 'Phase 1 - Tech Bootcamp',
            'start_date' => '2018-09-03',
            'end_date' => '2018-10-06',
        ]);

        DB::table('phases')->insert([
            'name' => 'Phase 2 - Business Bootcamp',
            'start_date' => '2018-10-08',
            'end_date' => '2018-10-31',
        ]);  	
    }
}
