<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GoalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {    
    	DB::table('goals')->insert([
            'user_id' => 2,
            'name' => 'Learn Classes and Ids',
            'progress' => 0,
            'start_date' => '2018-11-03',
            'end_date' => '2018-11-13',
        ]);

        DB::table('goals')->insert([
            'user_id' => 2,
            'name' => 'Work on presentation skills',
            'progress' => 0,
            'start_date' => '2018-12-03',
            'end_date' => '2018-12-23',
        ]);

        DB::table('goals')->insert([
            'user_id' => 2,
            'name' => 'Learn networking skills',
            'progress' => 10,
            'start_date' => '2018-10-03',
            'end_date' => '2018-10-23',
        ]);

        DB::table('goals')->insert([
            'user_id' => 2,
            'name' => 'Help others',
            'progress' => 80,
            'start_date' => '2018-09-03',
            'end_date' => '2018-09-23',
        ]);
    }
}
