<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
            'name' => 'Alacrity Admin',
            'email' => 'alacrity@admin.com',
            'password' => bcrypt('alacrity2018'),
            'jellyName' => '',
            'jellyColor' => '',
            'jellyType' => '',
            'jellyAge' => '',
        ]);

        DB::table('users')->insert([
            'name' => 'Kelly Jones',
            'email' => 'kjones@gmail.com',
            'password' => bcrypt('secret'),
            'jellyName' => 'JBean',
            'jellyColor' => 'pink',
            'jellyType' => 'normal',
            'jellyAge' => '',
        ]);

        DB::table('users')->insert([
            'name' => 'Emily Rees',
            'email' => 'erees@gmail.com',
            'password' => bcrypt('secret'),
            'jellyName' => 'Reeces Peices',
            'jellyColor' => 'oragne',
            'jellyType' => 'normal',
            'jellyAge' => '',
        ]);

        DB::table('users')->insert([
            'name' => 'Manie Maripise',
            'email' => 'mmaripise@gmail.com',
            'password' => bcrypt('secret'),
            'jellyName' => 'Harold',
            'jellyColor' => 'green',
            'jellyType' => 'hipster',
            'jellyAge' => '',
        ]);

        DB::table('users')->insert([
            'name' => 'Caleb Brown',
            'email' => 'cbrown@gmail.com',
            'password' => bcrypt('secret'),
            'jellyName' => 'Jiggly Pop',
            'jellyColor' => 'blue',
            'jellyType' => 'hipster',
            'jellyAge' => '',
        ]);

        DB::table('users')->insert([
            'name' => 'Jacob Berlyn',
            'email' => 'jberlyn@gmail.com',
            'password' => bcrypt('secret'),
            'jellyName' => 'J Berlyn',
            'jellyColor' => 'blue',
            'jellyType' => 'normal',
            'jellyAge' => '',
        ]);

        DB::table('users')->insert([
            'name' => 'Sam Bond',
            'email' => 'sbond@gmail.com',
            'password' => bcrypt('secret'),
            'jellyName' => 'Baked Bean',
            'jellyColor' => 'orange',
            'jellyType' => 'normal',
            'jellyAge' => '',
        ]);

         DB::table('users')->insert([
            'name' => 'Laurence Cullen',
            'email' => 'lcullen@gmail.com',
            'password' => bcrypt('secret'),
            'jellyName' => 'Greenie Beanie',
            'jellyColor' => 'green',
            'jellyType' => 'hipster',
            'jellyAge' => '',
        ]);

        DB::table('users')->insert([
            'name' => 'James Andrews',
            'email' => 'jandrew@gmail.com',
            'password' => bcrypt('secret'),
            'jellyName' => 'Bulky Bean',
            'jellyColor' => 'blue',
            'jellyType' => 'normal',
            'jellyAge' => '',
        ]);

        DB::table('users')->insert([
            'name' => 'Richard Morgan',
            'email' => 'rmorgan@gmail.com',
            'password' => bcrypt('secret'),
            'jellyName' => 'Richy Poo',
            'jellyColor' => 'pink',
            'jellyType' => 'hipster',
            'jellyAge' => '',
        ]);

        DB::table('users')->insert([
            'name' => 'Alex Long',
            'email' => 'along@gmail.com',
            'password' => bcrypt('secret'),
            'jellyName' => 'Longest Jelly',
            'jellyColor' => 'green',
            'jellyType' => 'normal',
            'jellyAge' => '',
        ]);
    }
}
