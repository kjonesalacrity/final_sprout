<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProgressionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

  
    public function run()
    {    
    	 DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 2,
           'progress' => 0,
           'timein' => 0,
           'updated' => '2018-09-10',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 2,
           'progress' => 50,
           'timein' => 20,
           'updated' => '2018-09-17',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 2,
           'timein' => 100,
           'progress' => 72,
           'updated' => '2018-09-27',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 2,
           'progress' => 84,
           'timein' => 150,
           'updated' => '2018-09-30',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 2,
           'progress' => 0,
           'timein' => 0,
           'updated' => '2018-10-01',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 2,
           'progress' => 20,
           'timein' => 20,
           'updated' => '2018-10-08',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 2,
           'progress' => 80,
           'timein' => 10,
           'updated' => '2018-10-18',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 2,
           'progress' => 99,
           'timein' => 50,
           'updated' => '2018-10-28',
        ]);

        //////////////////

        DB::table('progressions')->insert([
           'target_id' => 3,
           'user_id' => 2,
           'progress' => 0,
           'timein' => 0,
           'updated' => '2018-10-01',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 3,
           'user_id' => 2,
           'progress' => 20,
           'timein' => 20,
           'updated' => '2018-10-08',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 3,
           'user_id' => 2,
           'progress' => 80,
           'timein' => 10,
           'updated' => '2018-10-18',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 3,
           'user_id' => 2,
           'progress' => 100,
           'timein' => 50,
           'updated' => '2018-10-28',
        ]);

        ///////////////////

        DB::table('progressions')->insert([
           'target_id' => 4,
           'user_id' => 2,
           'progress' => 0,
           'timein' => 0,
           'updated' => '2018-10-01',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 4,
           'user_id' => 2,
           'progress' => 20,
           'timein' => 20,
           'updated' => '2018-10-08',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 4,
           'user_id' => 2,
           'progress' => 80,
           'timein' => 10,
           'updated' => '2018-10-18',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 4,
           'user_id' => 2,
           'progress' => 94,
           'timein' => 50,
           'updated' => '2018-10-28',
        ]);




        //Another user
        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 3,
           'progress' => 0,
           'timein' => 0,
           'updated' => '2018-09-10',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 3,
           'progress' => 24,
           'timein' => 40,
           'updated' => '2018-09-17',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 3,
           'timein' => 120,
           'progress' => 55,
           'updated' => '2018-09-27',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 3,
           'progress' => 97,
           'timein' => 200,
           'updated' => '2018-09-30',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 3,
           'progress' => 0,
           'timein' => 0,
           'updated' => '2018-10-01',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 3,
           'progress' => 40,
           'timein' => 100,
           'updated' => '2018-10-08',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 3,
           'progress' => 60,
           'timein' => 100,
           'updated' => '2018-10-18',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 3,
           'progress' => 100,
           'timein' => 120,
           'updated' => '2018-10-28',
        ]);

        //////////////////

        DB::table('progressions')->insert([
           'target_id' => 3,
           'user_id' => 3,
           'progress' => 0,
           'timein' => 0,
           'updated' => '2018-10-01',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 3,
           'user_id' => 3,
           'progress' => 20,
           'timein' => 20,
           'updated' => '2018-10-08',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 3,
           'user_id' => 3,
           'progress' => 80,
           'timein' => 10,
           'updated' => '2018-10-18',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 3,
           'user_id' => 3,
           'progress' => 100,
           'timein' => 50,
           'updated' => '2018-10-28',
        ]);

        ///////////////////

        DB::table('progressions')->insert([
           'target_id' => 4,
           'user_id' => 3,
           'progress' => 0,
           'timein' => 0,
           'updated' => '2018-10-01',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 4,
           'user_id' => 3,
           'progress' => 20,
           'timein' => 20,
           'updated' => '2018-10-08',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 4,
           'user_id' => 3,
           'progress' => 80,
           'timein' => 10,
           'updated' => '2018-10-18',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 4,
           'user_id' => 3,
           'progress' => 94,
           'timein' => 50,
           'updated' => '2018-10-28',
        ]);



        //Another user
        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 4,
           'progress' => 0,
           'timein' => 0,
           'updated' => '2018-09-10',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 4,
           'progress' => 10,
           'timein' => 10,
           'updated' => '2018-09-17',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 4,
           'timein' => 20,
           'progress' => 23,
           'updated' => '2018-09-27',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 4,
           'progress' => 40,
           'timein' => 200,
           'updated' => '2018-09-30',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 4,
           'progress' => 0,
           'timein' => 0,
           'updated' => '2018-10-01',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 4,
           'progress' => 53,
           'timein' => 55,
           'updated' => '2018-10-08',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 4,
           'progress' => 72,
           'timein' => 80,
           'updated' => '2018-10-18',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 4,
           'progress' => 100,
           'timein' => 120,
           'updated' => '2018-10-28',
        ]);



        //////
        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 5,
           'progress' => 0,
           'timein' => 0,
           'updated' => '2018-09-10',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 5,
           'progress' => 50,
           'timein' => 20,
           'updated' => '2018-09-17',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 5,
           'timein' => 100,
           'progress' => 72,
           'updated' => '2018-09-27',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 5,
           'progress' => 84,
           'timein' => 150,
           'updated' => '2018-09-30',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 5,
           'progress' => 0,
           'timein' => 0,
           'updated' => '2018-10-01',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 5,
           'progress' => 20,
           'timein' => 20,
           'updated' => '2018-10-08',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 5,
           'progress' => 80,
           'timein' => 10,
           'updated' => '2018-10-18',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 5,
           'progress' => 100,
           'timein' => 50,
           'updated' => '2018-10-28',
        ]);


        //Another user
        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 6,
           'progress' => 0,
           'timein' => 0,
           'updated' => '2018-09-10',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 6,
           'progress' => 24,
           'timein' => 40,
           'updated' => '2018-09-17',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 6,
           'timein' => 120,
           'progress' => 55,
           'updated' => '2018-09-27',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 6,
           'progress' => 97,
           'timein' => 200,
           'updated' => '2018-09-30',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 6,
           'progress' => 0,
           'timein' => 0,
           'updated' => '2018-10-01',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 6,
           'progress' => 40,
           'timein' => 100,
           'updated' => '2018-10-08',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 6,
           'progress' => 60,
           'timein' => 100,
           'updated' => '2018-10-18',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 6,
           'progress' => 100,
           'timein' => 120,
           'updated' => '2018-10-28',
        ]);


        //Another user
        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 7,
           'progress' => 0,
           'timein' => 0,
           'updated' => '2018-09-10',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 7,
           'progress' => 10,
           'timein' => 10,
           'updated' => '2018-09-17',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 7,
           'timein' => 20,
           'progress' => 23,
           'updated' => '2018-09-27',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 7,
           'progress' => 40,
           'timein' => 200,
           'updated' => '2018-09-30',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 7,
           'progress' => 0,
           'timein' => 0,
           'updated' => '2018-10-01',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 7,
           'progress' => 53,
           'timein' => 55,
           'updated' => '2018-10-08',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 7,
           'progress' => 72,
           'timein' => 80,
           'updated' => '2018-10-18',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 7,
           'progress' => 100,
           'timein' => 120,
           'updated' => '2018-10-28',
        ]);


        //////
        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 8,
           'progress' => 0,
           'timein' => 0,
           'updated' => '2018-09-10',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 8,
           'progress' => 50,
           'timein' => 20,
           'updated' => '2018-09-17',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 8,
           'timein' => 100,
           'progress' => 72,
           'updated' => '2018-09-27',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 8,
           'progress' => 84,
           'timein' => 150,
           'updated' => '2018-09-30',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 8,
           'progress' => 0,
           'timein' => 0,
           'updated' => '2018-10-01',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 8,
           'progress' => 20,
           'timein' => 20,
           'updated' => '2018-10-08',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 8,
           'progress' => 80,
           'timein' => 10,
           'updated' => '2018-10-18',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 8,
           'progress' => 100,
           'timein' => 50,
           'updated' => '2018-10-28',
        ]);


        //Another user
        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 9,
           'progress' => 0,
           'timein' => 0,
           'updated' => '2018-09-10',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 9,
           'progress' => 24,
           'timein' => 40,
           'updated' => '2018-09-17',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 9,
           'timein' => 120,
           'progress' => 55,
           'updated' => '2018-09-27',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 9,
           'progress' => 97,
           'timein' => 200,
           'updated' => '2018-09-30',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 9,
           'progress' => 0,
           'timein' => 0,
           'updated' => '2018-10-01',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 9,
           'progress' => 40,
           'timein' => 100,
           'updated' => '2018-10-08',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 9,
           'progress' => 60,
           'timein' => 100,
           'updated' => '2018-10-18',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 9,
           'progress' => 100,
           'timein' => 120,
           'updated' => '2018-10-28',
        ]);


        //Another user
        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 10,
           'progress' => 0,
           'timein' => 0,
           'updated' => '2018-09-10',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 10,
           'progress' => 10,
           'timein' => 10,
           'updated' => '2018-09-17',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 10,
           'timein' => 20,
           'progress' => 23,
           'updated' => '2018-09-27',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 1,
           'user_id' => 10,
           'progress' => 40,
           'timein' => 200,
           'updated' => '2018-09-30',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 10,
           'progress' => 0,
           'timein' => 0,
           'updated' => '2018-10-01',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 10,
           'progress' => 53,
           'timein' => 55,
           'updated' => '2018-10-08',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 10,
           'progress' => 72,
           'timein' => 80,
           'updated' => '2018-10-18',
        ]);

        DB::table('progressions')->insert([
           'target_id' => 2,
           'user_id' => 10,
           'progress' => 100,
           'timein' => 120,
           'updated' => '2018-10-28',
        ]);
    }
}
