<?php

use Illuminate\Database\Seeder;

class TargetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	 DB::table('targets')->insert([
           'name' => 'Learn HTML',
           'start_date' => '2018-10-01',
           'end_date' => '2018-10-01',
           'phase_id' => 1,
           'time_in' => 400,
        ]);

        DB::table('targets')->insert([
           'name' => 'Learn CSS and Bootstrap',
           'start_date' => '2018-10-01',
           'end_date' => '2018-10-01',
           'phase_id' => 1,
           'time_in' => 700,
        ]);

        DB::table('targets')->insert([
           'name' => 'Learn Javascript',
           'start_date' => '2018-10-01',
           'end_date' => '2018-10-01',
           'phase_id' => 1,
           'time_in' => 200,
        ]);

        DB::table('targets')->insert([
           'name' => 'Learn Php',
           'start_date' => '2018-10-01',
           'end_date' => '2018-10-01',
           'phase_id' => 1,
           'time_in' => 600,
        ]);

        DB::table('targets')->insert([
           'name' => 'Business Breakdown',
           'start_date' => '2018-10-01',
           'end_date' => '2018-10-01',
           'phase_id' => 2,
           'time_in' => 400,
        ]);

        DB::table('targets')->insert([
           'name' => 'Business Workshop',
           'start_date' => '2018-10-01',
           'end_date' => '2018-10-01',
           'phase_id' => 2,
           'time_in' => 700,
        ]);

        DB::table('targets')->insert([
           'name' => 'Presentation Talks',
           'start_date' => '2018-10-01',
           'end_date' => '2018-10-01',
           'phase_id' => 2,
           'time_in' => 200,
        ]);

        DB::table('targets')->insert([
           'name' => 'Talk about Business',
           'start_date' => '2018-10-01',
           'end_date' => '2018-10-01',
           'phase_id' => 2,
           'time_in' => 600,
        ]);
        
    }
}
